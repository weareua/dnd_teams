from graphene import relay, List, Int
from django.core.exceptions import ObjectDoesNotExist

from app.models.storyboard import StoryBoardRecord
from app.resolvers import types
from app.utils import get_campaign_from_user
from . import mutations


class StoryboardQuery(object):
    storyboard = relay.Node.Field(types.StoryBoardRecordNode)
    campaign_storyboard = List(
        types.StoryBoardRecordNode, first=Int(), skip=Int())

    def resolve_campaign_storyboard(self, info, first=None, skip=None):
        try:
            campaign = get_campaign_from_user(info.context.user)
            result = StoryBoardRecord.objects.filter(
                campaign=campaign).order_by(
                '-created_date')
            # pagination
            if skip:
                result = result[skip:]

            if first:
                result = result[:first]
            return result
        except ObjectDoesNotExist:
            return []


class StoryboardMutation(object):
    campaign_storyboard_record = mutations.StoryboardRecordMutation.Field()
    campaign_storyboard_record_delete = mutations.DeleteStoryboardRecordMutation.Field()  # noqa
    campaign_storyboard_comment = mutations.StoryboardCommentMutation.Field()
    campaign_storyboard_comment_delete = mutations.DeleteStoryboardRecordMutation.Field()  # noqa
