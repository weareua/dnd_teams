import graphene
from graphene.relay import ClientIDMutation
from graphql_relay.node.node import from_global_id
from datetime import datetime
from graphql import GraphQLError

from app.models.storyboard import StoryBoardRecord, StoryBoardComment
from app.resolvers import types
from app.utils import (get_campaign_from_user, get_character_from_user,
                       transliterate)


class StoryboardRecordMutation(ClientIDMutation):
    record = graphene.Field(types.StoryBoardRecordNode)

    class Input:
        id = graphene.ID()
        title = graphene.String()
        description = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            record = StoryBoardRecord.objects.get(pk=int(db_id))
            record.updated_date = datetime.now()
        else:
            # there are required fields for creation process
            required_fields = ('title', 'description', )
            if set(required_fields).issubset(input):
                record = StoryBoardRecord()
                record.campaign = get_campaign_from_user(info.context.user)
            else:
                raise GraphQLError('Create process validation error. These fields are requred: {0}'.format(  # noqa
                    ', '.join(required_fields)))

        if info.context.FILES:
            record.image = info.context.FILES['avatar']
            record.image.name = transliterate(record.image.name)

        record.edit(input)

        return StoryboardRecordMutation(record=record)


class DeleteStoryboardRecordMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        record = StoryBoardRecord.objects.get(pk=db_id)
        record.delete()
        return DeleteStoryboardRecordMutation(result=True)


class StoryboardCommentMutation(ClientIDMutation):
    comment = graphene.Field(types.StoryBoardCommentNode)

    class Input:
        id = graphene.ID()
        board_record_id = graphene.ID()
        text = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            comment = StoryBoardComment.objects.get(pk=int(db_id))
            if comment.author != get_character_from_user(info.context.user):
                raise GraphQLError('Only author can edit board comment')
            comment.updated_date = datetime.now()
            comment.edited = True
        else:
            # there are required fields for creation process
            required_fields = ('text', 'board_record_id')
            if set(required_fields).issubset(input):
                _, db_board_id = from_global_id(input['board_record_id'])
                comment = StoryBoardComment()
                comment.board_record = StoryBoardRecord.objects.get(
                    pk=int(db_board_id))
                comment.author = get_character_from_user(info.context.user)
                comment.campaign = get_campaign_from_user(info.context.user)
                # remove board_record_id before editing below
                input = {k: v for k, v in input.items() if k !=
                         'board_record_id'}
            else:
                raise GraphQLError('Create process validation error. These fields are requred: {0}'.format(  # noqa
                    ', '.join(required_fields)))

        comment.edit(input)

        return StoryboardCommentMutation(comment=comment)


class DeleteStoryboardCommentMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        comment = StoryBoardComment.objects.get(pk=db_id)
        comment.delete()
        return DeleteStoryboardCommentMutation(result=True)
