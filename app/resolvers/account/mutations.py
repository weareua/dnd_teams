import graphene
from graphene.relay import ClientIDMutation

from app.resolvers import types


class EmailMutation(ClientIDMutation):
    user = graphene.Field(types.UserNode)

    class Input:
        email = graphene.String(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        user = info.context.user
        user.email = input['email']
        user.save()

        return EmailMutation(user=user)


class DeleteUserMutation(ClientIDMutation):
    result = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info):
        user = info.context.user
        user.delete()
        return DeleteUserMutation(result=True)
