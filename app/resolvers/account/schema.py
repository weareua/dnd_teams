from graphene import Field

from app.resolvers import types
from . import mutations


class UserQuery(object):
    user = Field(types.UserNode)

    def resolve_user(self, info):
        return info.context.user


class UserMutation(object):
    user_edit_email = mutations.EmailMutation.Field()
    user_delete = mutations.DeleteUserMutation.Field()
