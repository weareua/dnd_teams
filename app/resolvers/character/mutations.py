import graphene
from graphene.relay import ClientIDMutation
from graphql import GraphQLError
from django.core.exceptions import ObjectDoesNotExist
from graphql_relay.node.node import from_global_id

from app.resolvers import types
from app.models.character.character import Character
from app.models.character.custom_fields import CustomField
from app.models.character.weapon import Weapon
from app.models.character.spell import Spell
from app.models.campaign import Campaign
from app.models.character.attributes import Attributes
from app.models.character.skills import Skills, SkillsTrained
from app.models.character.saving_throws import (
    SavingThrows, SavingThrowsTrained)
from app.models.character.characteristics import Characteristics
from app.utils import transliterate


class CreateCharacterMutation(ClientIDMutation):
    character = graphene.Field(types.CharacterNode)

    class Input:
        name = graphene.String(required=True)
        invitation_code = graphene.String(required=True)
        is_dm = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        campaign = None
        try:
            campaign = Campaign.objects.get(
                invitation_code=input['invitation_code'])
        except ObjectDoesNotExist:
            raise GraphQLError('Invitation code is invalid')
        if campaign:
            character = Character()
            character.name = input['name']
            character.user = info.context.user
            character.campaign = campaign
            if 'is_dm' in input:
                character.is_dm = input['is_dm']
            character.make_current(info.context.user)

            Attributes(character=character).save()
            Characteristics(character=character).save()
            Skills(character=character).save()
            SkillsTrained(character=character).save()
            SavingThrows(character=character).save()
            SavingThrowsTrained(character=character).save()

        return CreateCharacterMutation(character=character)


class CustomFieldMutation(ClientIDMutation):
    custom_field = graphene.Field(types.CustomFieldNode)

    class Input:
        id = graphene.ID()
        title = graphene.String(required=True)
        description = graphene.String(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            custom_field = CustomField.objects.get(pk=int(db_id))
        else:
            custom_field = CustomField()
            custom_field.character = info.context.user.character_set.get(
                current=True)
        custom_field.edit(input)
        return CustomFieldMutation(custom_field=custom_field)


class WeaponMutation(ClientIDMutation):
    weapon = graphene.Field(types.WeaponNode)

    class Input:
        id = graphene.ID()
        title = graphene.String(required=True)
        description = graphene.String(required=True)
        attack_bonus = graphene.String(required=True)
        damage = graphene.String(required=True)
        damage_type = graphene.String(required=True)
        distance = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            weapon = Weapon.objects.get(pk=int(db_id))
        else:
            weapon = Weapon()
            weapon.character = info.context.user.character_set.get(
                current=True)
        weapon.edit(input)
        return WeaponMutation(weapon=weapon)


class SpellMutation(ClientIDMutation):
    spell = graphene.Field(types.SpellNode)

    class Input:
        id = graphene.ID()
        title = graphene.String(required=True)
        description = graphene.String(required=True)
        level = graphene.String(required=True)
        cast_time = graphene.String(required=True)
        components = graphene.String()
        duration = graphene.String()
        distance = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            spell = Spell.objects.get(pk=int(db_id))
        else:
            spell = Spell()
            spell.character = info.context.user.character_set.get(
                current=True)
        spell.edit(input)
        return SpellMutation(spell=spell)


class DeleteCharacterMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        character = None
        _, db_id = from_global_id(input['id'])
        try:
            character = Character.objects.get(
                pk=db_id, user=info.context.user)
        except ObjectDoesNotExist:
            raise GraphQLError('Invalid id or user context')

        if character:
            character.delete()
            try:
                last_character = info.context.user.character_set.last()
                last_character.make_current(info.context.user)
            except (AttributeError, IndexError):
                pass
            return DeleteCharacterMutation(result=True)


class AttributesMutation(ClientIDMutation):
    attributes = graphene.Field(types.AttributesNode)

    class Input:
        id = graphene.ID(required=True)
        race = graphene.String()
        character_class = graphene.String()
        experience_points = graphene.Int()
        proficiency_bonus = graphene.Int()
        armor_class = graphene.Int()
        initiative = graphene.Int()
        speed = graphene.Int()
        current_hits = graphene.Int()
        total_hits = graphene.Int()
        hit_dices = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        attributes = Attributes.objects.get(pk=int(db_id))
        attributes.edit(input)
        return AttributesMutation(attributes=attributes)


class CharacteristicsMutation(ClientIDMutation):
    characteristics = graphene.Field(types.CharacteristicsNode)

    class Input:
        id = graphene.ID(required=True)
        strength = graphene.String()
        dexterity = graphene.String()
        constitution = graphene.String()
        intelligence = graphene.String()
        wisdom = graphene.String()
        charisma = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        characteristics = Characteristics.objects.get(pk=int(db_id))
        characteristics.edit(input)
        return CharacteristicsMutation(characteristics=characteristics)


class SkillsMutation(ClientIDMutation):
    skills = graphene.Field(types.SkillsNode)

    class Input:
        id = graphene.ID(required=True)
        acrobatics = graphene.Int()
        animal_handling = graphene.Int()
        arcana = graphene.Int()
        athletics = graphene.Int()
        deception = graphene.Int()
        history = graphene.Int()
        insight = graphene.Int()
        intimidation = graphene.Int()
        investigation = graphene.Int()
        medicine = graphene.Int()
        nature = graphene.Int()
        perception = graphene.Int()
        performance = graphene.Int()
        persuasion = graphene.Int()
        religion = graphene.Int()
        sleight_of_hand = graphene.Int()
        stealth = graphene.Int()
        survival = graphene.Int()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        skills = Skills.objects.get(pk=int(db_id))
        skills.edit(input)
        return SkillsMutation(skills=skills)


class SkillsTrainedMutation(ClientIDMutation):
    skills_trained = graphene.Field(types.SkillsTrainedNode)

    class Input:
        id = graphene.ID(required=True)
        acrobatics = graphene.Boolean()
        animal_handling = graphene.Boolean()
        arcana = graphene.Boolean()
        athletics = graphene.Boolean()
        deception = graphene.Boolean()
        history = graphene.Boolean()
        insight = graphene.Boolean()
        intimidation = graphene.Boolean()
        investigation = graphene.Boolean()
        medicine = graphene.Boolean()
        nature = graphene.Boolean()
        perception = graphene.Boolean()
        performance = graphene.Boolean()
        persuasion = graphene.Boolean()
        religion = graphene.Boolean()
        sleight_of_hand = graphene.Boolean()
        stealth = graphene.Boolean()
        survival = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        skills_trained = SkillsTrained.objects.get(pk=int(db_id))
        skills_trained.edit(input)
        return SkillsTrainedMutation(skills_trained=skills_trained)


class SavingThrowsMutation(ClientIDMutation):
    saving_throws = graphene.Field(types.SavingThrowsNode)

    class Input:
        id = graphene.ID(required=True)
        strength = graphene.Int()
        dexterity = graphene.Int()
        constitution = graphene.Int()
        intelligence = graphene.Int()
        wisdom = graphene.Int()
        charisma = graphene.Int()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        saving_throws = SavingThrows.objects.get(pk=int(db_id))
        saving_throws.edit(input)
        return SavingThrowsMutation(saving_throws=saving_throws)


class SavingThrowsTrainedMutation(ClientIDMutation):
    saving_throws_trained = graphene.Field(types.SavingThrowsTrainedNode)

    class Input:
        id = graphene.ID(required=True)
        strength = graphene.Boolean()
        dexterity = graphene.Boolean()
        constitution = graphene.Boolean()
        intelligence = graphene.Boolean()
        wisdom = graphene.Boolean()
        charisma = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        saving_throws_trained = SavingThrowsTrained.objects.get(pk=int(db_id))
        saving_throws_trained.edit(input)
        return SavingThrowsMutation(
            saving_throws_trained=saving_throws_trained)


class EssentialsMutation(ClientIDMutation):
    character = graphene.Field(types.CharacterNode)

    class Input:
        name = graphene.String()
        inventory = graphene.String()
        personal_notes = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        character = info.context.user.character_set.get(current=True)

        if info.context.FILES:
            character.avatar = info.context.FILES['avatar']
            character.avatar.name = transliterate(character.avatar.name)
        character.edit(input)

        return EssentialsMutation(character=character)
