from graphene import relay, List, Field

from app.models.character.character import Character
from app.resolvers import types
from . import mutations


class CharacterQuery(object):
    current_character = Field(types.CharacterNode)
    user_characters = List(types.CharacterNode)
    character = relay.Node.Field(types.CharacterNode)

    def resolve_current_character(self, info):
        return Character.objects.get(
            user=info.context.user, current=True)

    def resolve_user_characters(self, info):
        return Character.objects.filter(
            user=info.context.user).order_by('-created_date')


class CharacterMutation(object):
    character_create = mutations.CreateCharacterMutation.Field()
    character_delete = mutations.DeleteCharacterMutation.Field()

    character_custom_field = mutations.CustomFieldMutation.Field()
    character_weapon = mutations.WeaponMutation.Field()
    character_spell = mutations.SpellMutation.Field()
    character_attributes = mutations.AttributesMutation.Field()
    character_characteristics = mutations.CharacteristicsMutation.Field()
    character_skills = mutations.SkillsMutation.Field()
    character_skills_trained = mutations.SkillsTrainedMutation.Field()
    character_saving_throws = mutations.SavingThrowsMutation.Field()
    character_saving_throws_trained = mutations.SavingThrowsTrainedMutation.Field()  # noqa
    character_essentials = mutations.EssentialsMutation.Field()
