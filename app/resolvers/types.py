from graphene import relay, Field, List, ObjectType, String, Int
from graphene_django import DjangoObjectType

from app.models.campaign import Campaign
from app.models.character.character import Character
from app.models.character.attributes import Attributes
from app.models.character.skills import Skills, SkillsTrained
from app.models.character.saving_throws import (
    SavingThrows, SavingThrowsTrained)
from app.models.character.characteristics import Characteristics
from app.models.character.custom_fields import CustomField
from app.models.character.weapon import Weapon
from app.models.character.spell import Spell
from app.models.image import Image
from app.models.quest import Quest
from app.models.storyboard import StoryBoardRecord, StoryBoardComment
from landing.models import User


class LevelNode(ObjectType):
    experience = Int()
    current_level = Int()
    bonus = String()
    next_level_max_experience = Int()

    def resolve_current_level(self, info):
        return self.level

    def resolve_experience(self, info):
        return self.experience

    def resolve_bonus(self, info):
        return self.bonus

    def resolve_next_level_max_experience(self, info):
        return self.next


class AttributesNode(DjangoObjectType):
    level = Field(LevelNode)

    class Meta:
        model = Attributes
        interfaces = (relay.Node, )


class SkillsNode(DjangoObjectType):

    class Meta:
        model = Skills
        interfaces = (relay.Node, )


class SkillsTrainedNode(DjangoObjectType):

    class Meta:
        model = SkillsTrained
        interfaces = (relay.Node, )


class SavingThrowsNode(DjangoObjectType):

    class Meta:
        model = SavingThrows
        interfaces = (relay.Node, )


class SavingThrowsTrainedNode(DjangoObjectType):

    class Meta:
        model = SavingThrowsTrained
        interfaces = (relay.Node, )


class CharacteristicsNode(DjangoObjectType):

    class Meta:
        model = Characteristics
        interfaces = (relay.Node, )


class CustomFieldNode(DjangoObjectType):

    class Meta:
        model = CustomField
        interfaces = (relay.Node, )


class WeaponNode(DjangoObjectType):

    class Meta:
        model = Weapon
        interfaces = (relay.Node, )


class SpellNode(DjangoObjectType):

    class Meta:
        model = Spell
        interfaces = (relay.Node, )


class CharacterNode(DjangoObjectType):
    attributes = Field(AttributesNode)
    skills = Field(SkillsNode)
    skills_trained = Field(SkillsTrainedNode)
    saving_throws = Field(SavingThrowsNode)
    saving_throws_trained = Field(SavingThrowsTrainedNode)
    characteristics = Field(CharacteristicsNode)
    custom_fields = List(CustomFieldNode)
    weapons = List(WeaponNode)
    spells = List(SpellNode)

    class Meta:
        model = Character
        interfaces = (relay.Node, )

    def resolve_weapons(self, info):
        return self.weapons.all()

    def resolve_custom_fields(self, info):
        return self.custom_fields.all()

    def resolve_spells(self, info):
        return self.spells.all()


class CampaignNode(DjangoObjectType):
    characters = List(CharacterNode)

    class Meta:
        model = Campaign
        interfaces = (relay.Node, )

    def resolve_characters(self, info):
        return self.characters.all()


class ImageNode(DjangoObjectType):
    # GraphQL serializes Enum values as strings
    # Didn't manage how to get more fancy representation
    image_type = String()

    class Meta:
        model = Image
        interfaces = (relay.Node, )


class QuestNode(DjangoObjectType):
    # GraphQL serializes Enum values as strings
    # Didn't manage how to get more fancy representation
    quest_type = String()

    class Meta:
        model = Quest
        interfaces = (relay.Node, )


class StoryBoardCommentNode(DjangoObjectType):

    class Meta:
        model = StoryBoardComment
        interfaces = (relay.Node, )


class StoryBoardRecordNode(DjangoObjectType):
    comments = List(StoryBoardCommentNode)

    def resolve_comments(self, info):
        return self.comments.all()

    class Meta:
        model = StoryBoardRecord
        interfaces = (relay.Node, )


class UserNode(DjangoObjectType):

    class Meta:
        model = User
        interfaces = (relay.Node, )
