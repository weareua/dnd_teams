import graphene
from graphene.relay import ClientIDMutation
from graphql_relay.node.node import from_global_id
from graphql import GraphQLError

from app.models.image import Image
from app.enums import ImageTypeGraph, ImageType
from app.resolvers import types
from app.utils import transliterate, get_campaign_from_user


class ImageMutation(ClientIDMutation):
    image = graphene.Field(types.ImageNode)

    class Input:
        id = graphene.ID()
        title = graphene.String()
        description = graphene.String()
        image_type = ImageTypeGraph()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            image = Image.objects.get(pk=int(db_id))
        else:
            # there are required fields for creation process
            required_fields = ('title', 'image_type')
            if set(required_fields).issubset(input):
                image = Image()
                image.campaign = get_campaign_from_user(info.context.user)
            else:
                raise GraphQLError('Create process validation error. These fields are requred: {0}'.format(  # noqa
                    ', '.join(required_fields)))

        if info.context.FILES:
            image.image = info.context.FILES['avatar']
            image.image.name = transliterate(image.image.name)

        if 'image_type' in input:
            if ImageTypeGraph.M.value == input['image_type']:
                image_type_flag = ImageType.M
            else:
                image_type_flag = ImageType.A
            image.image_type = image_type_flag

            # we already handled image type, so we remove it from input attrs
            input = {k: v for k, v in input.items() if k != 'image_type'}

        image.edit(input)
        return ImageMutation(image=image)


class DeleteImageMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        image = Image.objects.get(pk=db_id)
        image.delete()
        return DeleteImageMutation(result=True)
