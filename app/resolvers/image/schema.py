from graphene import relay, List
from django.core.exceptions import ObjectDoesNotExist

from app.models.image import Image
from app.resolvers import types
from app.utils import get_campaign_from_user
from app.enums import ImageTypeGraph, ImageType
from . import mutations


class ImageQuery(object):
    image = relay.Node.Field(types.ImageNode)
    campaign_images = List(types.ImageNode, image_type=ImageTypeGraph())

    def resolve_campaign_images(self, info, image_type):
        image_type_flag = None
        if ImageType.M.value == image_type:
            image_type_flag = ImageType.M
        else:
            image_type_flag = ImageType.A
        try:
            campaign = get_campaign_from_user(info.context.user)
            return Image.objects.filter(
                campaign=campaign, image_type=image_type_flag).order_by(
                '-created_date')
        except ObjectDoesNotExist:
            return []


class ImageMutation(object):
    campaign_image = mutations.ImageMutation.Field()
    campaign_image_delete = mutations.DeleteImageMutation.Field()
