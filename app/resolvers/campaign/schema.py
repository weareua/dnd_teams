from graphene import relay, List, Field
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from app.models.campaign import Campaign
from app.resolvers import types
from .mutations import (CreateCampaignMutation,
                        EditCampaignMutation,
                        DeleteCampaignMutation)


class CampaignQuery(object):

    user_campaigns = List(types.CampaignNode)
    campaign = relay.Node.Field(types.CampaignNode)
    current_campaign = Field(types.CampaignNode)

    def resolve_current_campaign(self, info):
        campaign = None
        try:
            campaign = info.context.user.campaign_set.get(current=True)
        except (AttributeError, ObjectDoesNotExist):
            try:
                campaign = info.context.user.character_set.get(
                    current=True).campaign
            except (AttributeError, ObjectDoesNotExist):
                raise GraphQLError(
                    "There is no campaign to fetch for current user")
        return campaign

    def resolve_user_campaigns(self, info):
        return Campaign.objects.filter(
            creator=info.context.user).order_by('-created_date')


class CampaignMutation(object):
    campaign_create = CreateCampaignMutation.Field()
    campaign_edit = EditCampaignMutation.Field()
    campaign_delete = DeleteCampaignMutation.Field()
