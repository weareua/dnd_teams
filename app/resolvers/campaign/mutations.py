import graphene
from graphene.relay import ClientIDMutation
from graphql_relay.node.node import from_global_id

from app.resolvers.types import CampaignNode
from app.models.campaign import Campaign
from app.utils import invitation_code_gen


class CreateCampaignMutation(ClientIDMutation):
    campaign = graphene.Field(CampaignNode)

    class Input:
        title = graphene.String(required=True)
        description = graphene.String(required=True)
        twitch_channel_name = graphene.String()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        campaign = Campaign()
        campaign.creator = info.context.user
        campaign.invitation_code = invitation_code_gen()
        campaign.edit(input)
        campaign.make_current(info.context.user)

        return CreateCampaignMutation(campaign=campaign)


class EditCampaignMutation(ClientIDMutation):
    campaign = graphene.Field(CampaignNode)

    class Input:
        title = graphene.String()
        description = graphene.String()
        twitch_channel_name = graphene.String()
        revoke_invitation_code = graphene.Boolean()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        campaign = Campaign.objects.get(
            current=True, creator=info.context.user)
        campaign.edit(input)

        return EditCampaignMutation(campaign=campaign)


class DeleteCampaignMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        campaign = Campaign.objects.get(pk=db_id)
        campaign.delete()
        try:
            last_campaign = Campaign.objects.filter(
                creator=info.context.user).order_by('-created_date')[0]
            last_campaign.make_current(info.context.user)
        except IndexError:
            pass
        return DeleteCampaignMutation(result=True)
