import graphene
from graphene.relay import ClientIDMutation
from graphql_relay.node.node import from_global_id
from datetime import datetime
from graphql import GraphQLError

from app.models.quest import Quest
from app.enums import QuestTypeGraph, QuestType
from app.resolvers import types
from app.utils import get_campaign_from_user


class QuestMutation(ClientIDMutation):
    quest = graphene.Field(types.QuestNode)

    class Input:
        id = graphene.ID()
        title = graphene.String()
        description = graphene.String()
        quest_type = QuestTypeGraph()

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        if 'id' in input:
            _, db_id = from_global_id(input['id'])
            quest = Quest.objects.get(pk=int(db_id))
            quest.updated_date = datetime.now()
        else:
            # there are required fields for creation process
            required_fields = ('title', 'description', 'quest_type')
            if set(required_fields).issubset(input):
                quest = Quest()
                quest.campaign = get_campaign_from_user(info.context.user)
            else:
                raise GraphQLError('Create process validation error. These fields are requred: {0}'.format(  # noqa
                    ', '.join(required_fields)))

        if 'quest_type' in input:
            quest_type_flag = None
            if QuestType.A.value == input['quest_type']:
                quest_type_flag = QuestType.A
            elif QuestType.C.value == input['quest_type']:
                quest_type_flag = QuestType.C
            elif QuestType.F.value == input['quest_type']:
                quest_type_flag = QuestType.F
            else:
                quest_type_flag = QuestType.N
            quest.quest_type = quest_type_flag

            # we already handled quest type, so we remove it from input attrs
            input = {k: v for k, v in input.items() if k != 'quest_type'}

        quest.edit(input)

        return QuestMutation(quest=quest)


class DeleteQuestMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        quest = Quest.objects.get(pk=db_id)
        quest.delete()
        return DeleteQuestMutation(result=True)
