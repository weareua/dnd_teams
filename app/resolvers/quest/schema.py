from graphene import relay, List, Int
from django.core.exceptions import ObjectDoesNotExist

from app.models.quest import Quest
from app.resolvers import types
from app.utils import get_campaign_from_user
from app.enums import QuestTypeGraph, QuestType
from . import mutations


class QuestQuery(object):
    quest = relay.Node.Field(types.QuestNode)
    campaign_quests = List(
        types.QuestNode, quest_type=QuestTypeGraph(), first=Int(), skip=Int())
    campaign_quests_all = List(types.QuestNode)

    def resolve_campaign_quests(self, info, quest_type, first=None, skip=None):
        quest_type_flag = None
        if QuestType.A.value == quest_type:
            quest_type_flag = QuestType.A
        elif QuestType.C.value == quest_type:
            quest_type_flag = QuestType.C
        elif QuestType.F.value == quest_type:
            quest_type_flag = QuestType.F
        else:
            quest_type_flag = QuestType.N
        try:
            campaign = get_campaign_from_user(info.context.user)
            result = Quest.objects.filter(
                campaign=campaign, quest_type=quest_type_flag).order_by(
                '-created_date')
            # pagination
            if skip:
                result = result[skip:]

            if first:
                result = result[:first]
            return result

        except ObjectDoesNotExist:
            return []

    def resolve_campaign_quests_all(self, info):
        try:
            campaign = get_campaign_from_user(info.context.user)
            return Quest.objects.filter(
                campaign=campaign).order_by(
                '-created_date')

        except ObjectDoesNotExist:
            return []


class QuestMutation(object):
    campaign_quest = mutations.QuestMutation.Field()
    campaign_quest_delete = mutations.DeleteQuestMutation.Field()
