from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.character.character import Character
from app.utils import EditModelMixin


class SavingThrows(EditModelMixin, models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    strength = models.IntegerField(_('strength'), default=0)
    dexterity = models.IntegerField(_('dexterity'), default=0)
    constitution = models.IntegerField(_('constitution'), default=0)
    intelligence = models.IntegerField(_('intelligence'), default=0)
    wisdom = models.IntegerField(_('wisdom'), default=0)
    charisma = models.IntegerField(_('charisma'), default=0)


class SavingThrowsTrained(models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    strength = models.BooleanField(_('strength'), default=False)
    dexterity = models.BooleanField(_('dexterity'), default=False)
    constitution = models.BooleanField(_('constitution'), default=False)
    intelligence = models.BooleanField(_('intelligence'), default=False)
    wisdom = models.BooleanField(_('wisdom'), default=False)
    charisma = models.BooleanField(_('charisma'), default=False)

    def edit(self, throws):
        for throw in throws:
            throw = throw.lower().replace(' ', '_')
            if throw in self.__dict__:
                self.__dict__.update({throw: True})
        self.save()
