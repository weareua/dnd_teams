from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.character.character import Character
from app.utils import EditModelMixin


class Skills(EditModelMixin, models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    acrobatics = models.IntegerField(_('acrobatics'), default=0)
    animal_handling = models.IntegerField(_('animal_handling'), default=0)
    arcana = models.IntegerField(_('arcana'), default=0)
    athletics = models.IntegerField(_('athletics'), default=0)
    deception = models.IntegerField(_('deception'), default=0)
    history = models.IntegerField(_('history'), default=0)
    insight = models.IntegerField(_('insight'), default=0)
    intimidation = models.IntegerField(_('intimidation'), default=0)
    investigation = models.IntegerField(_('investigation'), default=0)
    medicine = models.IntegerField(_('medicine'), default=0)
    nature = models.IntegerField(_('nature'), default=0)
    perception = models.IntegerField(_('perception'), default=0)
    performance = models.IntegerField(_('performance'), default=0)
    persuasion = models.IntegerField(_('persuasion'), default=0)
    religion = models.IntegerField(_('religion'), default=0)
    sleight_of_hand = models.IntegerField(_('sleight_of_hand'), default=0)
    stealth = models.IntegerField(_('stealth'), default=0)
    survival = models.IntegerField(_('survival'), default=0)


class SkillsTrained(models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    acrobatics = models.BooleanField(_('acrobatics'), default=False)
    animal_handling = models.BooleanField(_('animal_handling'), default=False)
    arcana = models.BooleanField(_('arcana'), default=False)
    athletics = models.BooleanField(_('athletics'), default=False)
    deception = models.BooleanField(_('deception'), default=False)
    history = models.BooleanField(_('history'), default=False)
    insight = models.BooleanField(_('insight'), default=False)
    intimidation = models.BooleanField(_('intimidation'), default=False)
    investigation = models.BooleanField(_('investigation'), default=False)
    medicine = models.BooleanField(_('medicine'), default=False)
    nature = models.BooleanField(_('nature'), default=False)
    perception = models.BooleanField(_('perception'), default=False)
    performance = models.BooleanField(_('performance'), default=False)
    persuasion = models.BooleanField(_('persuasion'), default=False)
    religion = models.BooleanField(_('religion'), default=False)
    sleight_of_hand = models.BooleanField(_('sleight_of_hand'), default=False)
    stealth = models.BooleanField(_('stealth'), default=False)
    survival = models.BooleanField(_('survival'), default=False)

    def edit(self, skills):
        for skill in skills:
            skill = skill.lower().replace(' ', '_')
            if skill in self.__dict__:
                self.__dict__.update({skill: True})
        self.save()
