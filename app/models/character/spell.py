from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.character.character import Character
from app.utils import EditModelMixin


class Spell(EditModelMixin, models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name='spells')  # noqa
    title = models.CharField(_('title'), max_length=50)
    description = models.TextField(_('description'), )
    level = models.CharField(_('level'), max_length=80)
    cast_time = models.CharField(_('cast_time'), max_length=100)
    components = models.CharField(
        _('components'), max_length=150, null=True, blank=True)
    duration = models.CharField(
        _('duration'), max_length=50, null=True, blank=True)
    distance = models.CharField(
        _('distance'), max_length=50, null=True, blank=True)

    def __str__(self):
        return self.title
