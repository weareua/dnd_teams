from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.character.character import Character
from app.utils import EditModelMixin


class Characteristics(EditModelMixin, models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    strength = models.CharField(
        _('strength'), max_length=10, null=True, blank=True)
    dexterity = models.CharField(
        _('dexterity'), max_length=10, null=True, blank=True)
    constitution = models.CharField(
        _('constitution'), max_length=10, null=True, blank=True)
    intelligence = models.CharField(
        _('intelligence'), max_length=10, null=True, blank=True)
    wisdom = models.CharField(
        _('wisdom'), max_length=10, null=True, blank=True)
    charisma = models.CharField(
        _('charisma'), max_length=10, null=True, blank=True)
