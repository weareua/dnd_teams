from django.db import models
from django.utils.translation import gettext_lazy as _

from app.utils import get_profile_level_and_bonus, EditModelMixin
from app.models.character.character import Character


class Attributes(EditModelMixin, models.Model):
    character = models.OneToOneField(
        Character,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    race = models.CharField(_('race'), max_length=50, null=True, blank=True)
    character_class = models.CharField(_('character_class'), max_length=50,
                                       null=True, blank=True)
    experience_points = models.IntegerField(_('experience_points'), default=0)
    proficiency_bonus = models.IntegerField(_('proficiency_bonus'), default=0)
    armor_class = models.IntegerField(_('armor_class'), default=0)
    initiative = models.IntegerField(_('initiative'), default=0)
    speed = models.IntegerField(_('speed'), default=0)
    current_hits = models.IntegerField(_('current_hits'), default=0)
    total_hits = models.IntegerField(_('total_hits'), default=0)
    hit_dices = models.CharField(
        _('hit_dices'), max_length=50, null=True, blank=True)

    @property
    def level(self):
        return get_profile_level_and_bonus(int(self.experience_points))
