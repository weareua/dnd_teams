from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from app.models.campaign import Campaign
from app.utils import EditModelMixin


class CharacterManager(models.Manager):

    def get_by_natural_key(self, name, avatar):
        return self.get(name=name, avatar=avatar.url)


class Character(EditModelMixin, models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE,
        related_name='characters', blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE)
    name = models.CharField(_('name'), max_length=200)
    avatar = models.ImageField(
        upload_to='avatars/',
        default='avatars/placeholder.jpg')
    current = models.BooleanField(default=False)
    is_dm = models.BooleanField(default=False)
    inventory = models.TextField(null=True, blank=True)
    personal_notes = models.TextField(null=True, blank=True)
    created_date = models.DateTimeField(default=timezone.now)

    objects = CharacterManager()

    def make_current(self, user):
        characters = Character.objects.filter(user=user)
        for character in characters:
            character.current = False
            character.save()

        self.current = True
        self.save()

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.name, self.avatar.url)

    class Meta:
        unique_together = (('name', 'avatar'),)
