from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.character.character import Character
from app.utils import EditModelMixin


class Weapon(EditModelMixin, models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name='weapons')
    title = models.CharField(_('title'), max_length=30)
    description = models.TextField(_('description'), null=True, blank=True)
    attack_bonus = models.CharField(_('attack_bonus'), max_length=30)
    damage = models.CharField(_('damage'), max_length=30)
    damage_type = models.CharField(_('damage_type'), max_length=30)
    distance = models.CharField(
        _('distance'), max_length=30, null=True, blank=True)

    def __str__(self):
        return self.title
