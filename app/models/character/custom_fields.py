from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.character.character import Character
from app.utils import EditModelMixin


class CustomField(EditModelMixin, models.Model):
    character = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name='custom_fields')  # noqa
    title = models.CharField(_('title'), max_length=30)
    description = models.TextField(_('description'),)

    def __str__(self):
        return self.title
