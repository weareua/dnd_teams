from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from app.utils import EditModelMixin


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class Campaign(EditModelMixin, models.Model):
    title = models.CharField(_('title'), max_length=200)
    description = models.TextField(_('description'),)
    invitation_code = models.CharField(max_length=50)
    created_date = models.DateTimeField(default=timezone.now)
    current = models.BooleanField(default=False)
    twitch_channel_name = models.CharField(
        _('twitch_channel_name'),
        max_length=50, blank=True, null=True)
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET(get_sentinel_user),
    )

    def make_current(self, user):
        # disable all other campaigns
        campaigns = Campaign.objects.filter(creator=user)
        for camp in campaigns:
            camp.current = False
            camp.save()

        self.current = True
        self.save()

    def __str__(self):
        return self.title
