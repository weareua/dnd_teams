from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.campaign import Campaign
from app.enums import QuestType
from app.utils import EditModelMixin


class Quest(EditModelMixin, models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='quests')
    title = models.CharField(_('title'), max_length=200)
    description = models.TextField(_('description'), )
    quest_type = models.CharField(
        _('quest_type'),
        max_length=1,
        choices=[
            (q_type, q_type.value) for q_type in QuestType],
        default=QuestType.A)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title
