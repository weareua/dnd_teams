from django.db import models
from django.utils.translation import gettext_lazy as _

from app.models.campaign import Campaign
from app.enums import ImageType
from app.utils import EditModelMixin


class Image(EditModelMixin, models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='images')
    title = models.CharField(_('title'), max_length=200)
    description = models.TextField(_('description'), )
    image = models.ImageField(upload_to='images/')
    image_type = models.CharField(
        _('image_type'),
        max_length=1,
        choices=[
            (i_type, i_type.value) for i_type in ImageType],
        default=ImageType.A)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
