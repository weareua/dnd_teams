from django.db import models


from app.models.campaign import Campaign
from app.models.character.character import Character
from app.utils import EditModelMixin


class StoryBoardRecord(EditModelMixin, models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='board_records')
    title = models.CharField(max_length=200)
    description = models.TextField()
    image = models.ImageField(
        upload_to='storyboards/')
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title


class StoryBoardComment(EditModelMixin, models.Model):
    board_record = models.ForeignKey(
        StoryBoardRecord, on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(
        Character, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField()
    edited = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        # return author and firt 10 symbols of comment
        return "{0}: {1}...".format(self.author, self.text[:15])
