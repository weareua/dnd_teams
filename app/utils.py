import string
import random
from django.utils import timezone
from collections import namedtuple
from transliterate import translit, detect_language
from django.core.exceptions import ObjectDoesNotExist
from graphql import GraphQLError

from core.settings import INVITATION_CODE_LENGTH


class EditModelMixin(object):

    def edit(self, attrs, updated_date=False):
        for attr in attrs:
            if attr in self.__dict__ and attr is not 'id':
                self.__dict__.update({attr: attrs[attr]})
        if updated_date:
            self.updated_date = timezone.now()
        self.save()


def invitation_code_gen(
        length=INVITATION_CODE_LENGTH,
        chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(length))


def get_profile_level_and_bonus(experience_points):
    levels_list = []
    Level = namedtuple("Level", "experience level bonus")
    EnhancedLevel = namedtuple("EnhancedLevel", "experience level bonus next",)
    levels_list.append(Level(0, 1, +2))
    levels_list.append(Level(300, 2, +2))
    levels_list.append(Level(900, 3, +2))
    levels_list.append(Level(2700, 4, +2))
    levels_list.append(Level(6500, 5, +3))
    levels_list.append(Level(14000, 6, +3))
    levels_list.append(Level(23000, 7, +3))
    levels_list.append(Level(34000, 8, +3))
    levels_list.append(Level(48000, 9, +4))
    levels_list.append(Level(64000, 10, +4))
    levels_list.append(Level(85000, 11, +4))
    levels_list.append(Level(100000, 12, +4))
    levels_list.append(Level(120000, 13, +5))
    levels_list.append(Level(140000, 14, +5))
    levels_list.append(Level(165000, 15, +5))
    levels_list.append(Level(195000, 16, +5))
    levels_list.append(Level(225000, 17, +6))
    levels_list.append(Level(265000, 18, +6))
    levels_list.append(Level(305000, 19, +6))
    levels_list.append(Level(355000, 20, +6))

    if experience_points > 0:
        current_level = [level for level in levels_list if level.experience < experience_points][-1]  # noqa
    else:
        current_level = levels_list[0]
    next_level = levels_list[levels_list.index(current_level) + 1]
    return EnhancedLevel(
        current_level.experience,
        current_level.level, current_level.bonus, next_level.experience)


def transliterate(text):
    lang = detect_language(text)
    if lang:
        return translit(text, lang, reversed=True)
    return text


def get_character_from_user(user):
    try:
        return user.character_set.get(current=True)
    except ObjectDoesNotExist:
        raise GraphQLError("User has no current character")


def get_campaign_from_user(user):
    character = get_character_from_user(user)

    try:
        return character.campaign
    except ObjectDoesNotExist:
        raise GraphQLError("Current character isn't related to any campaign")
