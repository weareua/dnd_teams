from enum import Enum
from graphene import Enum as GrapheneEnum


class QuestType(Enum):
    A = "Active"
    C = "Completed"
    F = "Failed"
    N = "Notes"


class ImageType(Enum):
    A = "Artwork"
    M = "Maps"


# we are forced to recreate ImageType enum in order to avoid AssertionError
# https://github.com/graphql-python/graphene/issues/273
class ImageTypeGraph(GrapheneEnum):
    A = "Artwork"
    M = "Maps"


# we are forced to recreate ImageType enum in order to avoid AssertionError
# https://github.com/graphql-python/graphene/issues/273
class QuestTypeGraph(GrapheneEnum):
    A = "Active"
    C = "Completed"
    F = "Failed"
    N = "Notes"
