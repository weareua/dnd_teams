# Generated by Django 2.1.5 on 2019-02-20 09:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20190220_0926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storyboardrecord',
            name='created_date',
            field=models.DateTimeField(),
        ),
    ]
