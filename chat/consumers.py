import json
from datetime import timedelta
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async
from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.core import serializers
from django.utils.translation import gettext_lazy as _
from django.utils.text import format_lazy

from app.models.character import Character
from chat.models import ChatMessage
from chat.utils import roll_dice
from core.settings import DICE_AVATAR


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_group_name = 'chat'
        self.user = self.scope["user"]
        self.character = await self.get_character_by_user()

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name,
        )
        # Join room group
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        if 'message' in text_data_json:
            message = text_data_json['message']

            # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'chat_message',
                    'message': message,
                    'character_id': self.character.id
                }
            )
        else:
            roll_result = (
                roll_dice(text_data_json['dice_qty'], text_data_json[
                    'dice_type'], text_data_json['dice_mod']))
            # Send message to room group
            await self.channel_layer.group_send(

                self.room_group_name,
                {
                    'type': 'chat_message',
                    'character_id': self.character.id,
                    'roll_result': roll_result,
                }
            )

    # Receive message from room group
    async def chat_message(self, event):
        character = await self.get_character_by_id(event['character_id'])
        if 'message' in event:
            message = event['message']
            # Send message to WebSocket
            await self.send(text_data=json.dumps({
                'message': message,
                'character_id': character.id,
                'character_name': character.name,
                'avatar_url': character.avatar.url}))
            await self.save_message(message)
        else:
            # Send message to WebSocket
            throws_a_dice = _('throws a dice')
            dice_name = _('Dice')
            message = format_lazy(
                '{name} {throws_a_dice}: {roll_result}',
                name=character.name, throws_a_dice=throws_a_dice,
                roll_result=event['roll_result'])

            await self.send(text_data=json.dumps({
                'message': str(message),
                'dice': True,
                'character_name': str(dice_name),
                'avatar_url': DICE_AVATAR}))
            await self.save_message(message, dice=True)

    @database_sync_to_async
    def get_character_by_id(self, character_id):
        return Character.objects.get(pk=character_id)

    @database_sync_to_async
    def get_character_by_user(self):
        return self.user.character_set.get(current=True)

    @database_sync_to_async
    def save_message(self, message_body, dice=False):
        message = ChatMessage()
        message.text = message_body
        if not dice:
            message.author = self.character
        message.campaign = self.character.campaign
        message.save()
