from graphene import relay, List, Int
from django.core.exceptions import ObjectDoesNotExist


from chat.models import ChatMessage
from chat.types import ChatMessageNode
from app.utils import get_campaign_from_user
from chat import mutations


class ChatQuery(object):
    chat_message = relay.Node.Field(ChatMessageNode)
    chat_messages = List(ChatMessageNode, first=Int(), skip=Int())

    def resolve_chat_messages(self, info, first=None, skip=None):

        try:
            campaign = get_campaign_from_user(info.context.user)
            result = ChatMessage.objects.filter(
                campaign=campaign).order_by('-created_date')
            # pagination
            if skip:
                result = result[skip:]

            if first:
                result = result[:first]
            return result
        except ObjectDoesNotExist:
            return []


class ChatMutation(object):
    chat_message = mutations.ChatMessageMutation.Field()
    chat_message_delete = mutations.DeleteChatMessageMutation.Field()
