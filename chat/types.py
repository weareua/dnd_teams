from graphene import relay
from graphene_django import DjangoObjectType

from chat.models import ChatMessage


class ChatMessageNode(DjangoObjectType):

    class Meta:
        model = ChatMessage
        interfaces = (relay.Node, )
