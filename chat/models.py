
from django.contrib.auth import get_user_model
from django.db import models

from app.models.campaign import Campaign
from app.models.character.character import Character


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class ChatMessage(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='chat_messages')
    author = models.ForeignKey(
        Character, on_delete=models.SET(
            get_sentinel_user),
        related_name='chat_messages', blank=True, null=True)
    text = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
