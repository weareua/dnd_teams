import random


def roll_dice(quantity, initial_dice, modifier):
    """
    Roll the dice. Returns result of quantity, dice type and modifier
    """
    quantity = parseint(quantity)
    dice = int(initial_dice.replace("D", ""))
    modifier = parseint(modifier)
    results_list = []
    dice_result = 0
    message = ""

    if quantity:
        for i in range(quantity):
            dice_result = random.randint(1, dice)
            results_list.append(dice_result)
    else:
        dice_result = random.randint(1, dice)
        results_list.append(dice_result)

    result = sum(results_list)

    if modifier:
        result = result + modifier

    if not quantity or quantity == 1:
        message = "|{0}|".format(dice_result)
    else:
        for number in results_list:
            message += "|{0}| + ".format(number)
        message = message[:-2]
    if modifier:
        message += " + {0} ".format(modifier)
    if (quantity and quantity != 1) or modifier:
        message += "= {0}".format(result)
    return(message)


def parseint(x):
    temp = []
    for c in str(x).strip():
        if c.isdigit():
            temp.append(c)
        else:
            break
    return int("".join(temp)) if "".join(temp) else None
