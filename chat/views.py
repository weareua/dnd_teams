import json
from datetime import datetime, timedelta
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.core.exceptions import ObjectDoesNotExist

from chat.models import ChatMessage
from core.settings import WS_PROTOCOL


def index(request):
    try:
        character = request.user.character_set.get(current=True)
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse_lazy('characters_list'))

    current_date = datetime.today().strftime('%Y-%m-%d')
    actual_messages = ChatMessage.objects.filter(
        created_date__contains=current_date).order_by('created_date')

    previous_date = (datetime.today() - timedelta(days=1)).strftime('%Y-%m-%d')
    previous_mesage = ChatMessage.objects.filter(
        created_date__lt=previous_date)[0]
    historical_date = (previous_mesage.created_date -
                       timedelta(days=1)).strftime('%Y-%m-%d')

    historical_messages = ChatMessage.objects.filter(
        created_date__range=[historical_date, current_date]).order_by(
        'created_date')

    return render(request, 'chat/index.html', {
        'character_id': mark_safe(json.dumps(character.id)),
        'actual_messages': actual_messages,
        'historical_messages': historical_messages,
        'current_date': current_date,
        'WS_PROTOCOL': WS_PROTOCOL
    })
