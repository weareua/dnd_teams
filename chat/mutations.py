import graphene
from graphene.relay import ClientIDMutation
from graphql_relay.node.node import from_global_id

from chat.models import ChatMessage
from chat.types import ChatMessageNode
from app.utils import get_campaign_from_user, get_character_from_user


class ChatMessageMutation(ClientIDMutation):
    chat_message = graphene.Field(ChatMessageNode)

    class Input:
        text = graphene.String(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        chat_message = ChatMessage()
        chat_message.text = input['text']
        chat_message.author = get_character_from_user(info.context.user)
        chat_message.campaign = get_campaign_from_user(info.context.user)
        chat_message.save()

        return ChatMessageMutation(chat_message=chat_message)


class DeleteChatMessageMutation(ClientIDMutation):
    result = graphene.Boolean()

    class Input:
        id = graphene.ID(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        _, db_id = from_global_id(input['id'])
        chat_message = ChatMessage.objects.get(pk=int(db_id))
        chat_message.delete()
        return DeleteChatMessageMutation(result=True)
