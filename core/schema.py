import graphene
import graphql_jwt

from app.resolvers.character.schema import CharacterQuery, CharacterMutation
from app.resolvers.campaign.schema import CampaignQuery, CampaignMutation
from app.resolvers.image.schema import ImageQuery, ImageMutation
from app.resolvers.quest.schema import QuestQuery, QuestMutation
from app.resolvers.storyboard.schema import StoryboardQuery, StoryboardMutation
from app.resolvers.account.schema import UserQuery, UserMutation
from chat.schema import ChatQuery, ChatMutation
from landing.schema import AuthQuery, AuthMutation


class Query(AuthQuery, UserQuery, ChatQuery, StoryboardQuery, QuestQuery,
            ImageQuery, CharacterQuery, CampaignQuery, graphene.ObjectType):
    pass


class Mutation(AuthMutation, UserMutation, ChatMutation, StoryboardMutation,
               QuestMutation, ImageMutation, CharacterMutation,
               CampaignMutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
