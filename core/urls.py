from django.urls import path, include, re_path
from django.views.static import serve
from graphene_django.views import GraphQLView

from core.env_settings import MEDIA_ROOT

urlpatterns = [
    re_path(r'^media/(?P<path>.*)$', serve,
            {'document_root': MEDIA_ROOT}),
    re_path(r'^graphql', GraphQLView.as_view(graphiql=True), name='api'),
    path('chat/', include('chat.urls')),
    path('accounts/', include('landing.urls')),
    path('', include('app.urls')),
]
