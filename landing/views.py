from django.contrib.auth import views
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


class CustomPasswordResetDoneView(views.PasswordResetDoneView):

    def post(self, form):
        return HttpResponseRedirect(reverse_lazy('login'))


class CustomPasswordResetCompleteView(views.PasswordResetCompleteView):

    def post(self, form):
        return HttpResponseRedirect(reverse_lazy('login'))
