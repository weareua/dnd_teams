from graphene import Boolean, Field, String
from graphql_jwt.utils import jwt_encode, jwt_payload

from landing import mutations
from landing.models import User


class AuthQuery(object):
    showcase_token = Field(String, is_dm=Boolean())

    def resolve_showcase_token(self, info, is_dm=False):
        user = User.objects.filter(
            options__is_dm=is_dm, options__is_demo=True)[0]
        payload = jwt_payload(user)
        return jwt_encode(payload)


class AuthMutation(object):
    register = mutations.RegisterMutation.Field()
