from django.contrib.auth import views
from django.urls import path
from stronghold.decorators import public

from landing import views as landing_views

urlpatterns = [
    path(
        'password-reset/',
        views.PasswordResetView.as_view(), name='password_reset'),
    path('password-reset/done/',
         landing_views.CustomPasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/', public(
        views.PasswordResetConfirmView.as_view()),
        name='password_reset_confirm'),
    path('reset/done/',
         landing_views.CustomPasswordResetCompleteView.as_view(),
         name='password_reset_complete'),
]
