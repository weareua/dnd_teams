import graphene
from graphene.relay import ClientIDMutation
from graphql_jwt.utils import jwt_encode, jwt_payload

from landing.models import Options, User


class RegisterMutation(ClientIDMutation):
    token = graphene.String()

    class Input:
        email = graphene.String(required=True)
        password = graphene.String(required=True)
        is_dm = graphene.Boolean(required=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        user = User()
        user.email = input['email']
        user.set_password(input['password'])
        user.save()
        Options(user=user, is_dm=input['is_dm']).save()
        payload = jwt_payload(user)
        token = jwt_encode(payload)

        return RegisterMutation(token=token)
